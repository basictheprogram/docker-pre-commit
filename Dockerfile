FROM golang:bullseye
RUN apt update && apt install -y --no-install-recommends git pip && rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true
RUN pip install --no-cache-dir pre-commit
